package main

import (
	"log"
	"os"
	"time"

	"github.com/patrickmn/go-cache"

	"gitlab.com/pedroteosousa/fritando/bot"
	"gitlab.com/pedroteosousa/fritando/codeforces"
	"gitlab.com/pedroteosousa/fritando/commands"
)

func main() {
	var err error
	logFile, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	logger := log.New(logFile, "", log.Ldate|log.Ltime|log.Lshortfile)

	cache := cache.New(1*time.Hour, 1*time.Hour)

	var codeforcesAPI codeforces.CodeforcesAPI = codeforces.NewCodeforcesAPIClient(cache)

	var chatBot bot.Bot = bot.NewTelegramBot(logger)

	commands.NewFindContest(chatBot, codeforcesAPI, logger)

	chatBot.Start()
}
