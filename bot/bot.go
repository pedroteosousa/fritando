package bot

import (
	"fmt"
	"log"
	"os"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type Command func(chatID int64, arguments ...string) error

type Bot interface {
	SendMessage(chatID int64, text string) error
	SetCommand(name string, command Command) error
	Start() error
}

type TelegramBot struct {
	api      *tgbotapi.BotAPI
	logger   *log.Logger
	commands map[string]Command
}

func NewTelegramBot(logger *log.Logger) *TelegramBot {
	api, err := tgbotapi.NewBotAPI(os.Getenv("BOT_TOKEN"))
	if err != nil {
		logger.Printf("error while starting telegram bot: %v", err)
	}
	return &TelegramBot{
		api:      api,
		logger:   logger,
		commands: make(map[string]Command),
	}
}

func (bot *TelegramBot) SendMessage(chatID int64, text string) error {
	msg := tgbotapi.NewMessage(chatID, text)
	msg.ParseMode = "html"
	msg.DisableWebPagePreview = true
	_, err := bot.api.Send(msg)
	return err
}

func (bot *TelegramBot) SetCommand(name string, command Command) error {
	if _, ok := bot.commands[name]; ok {
		return fmt.Errorf("command %v already exists", name)
	}
	bot.commands[name] = command
	return nil
}

func (bot *TelegramBot) Start() error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.api.GetUpdatesChan(u)
	if err != nil {
		return err
	}

	for update := range updates {
		if update.Message.IsCommand() {
			arguments := update.Message.CommandArguments()
			commandText := update.Message.Command()
			command, ok := bot.commands[commandText]
			if ok {
				var err error
				if len(arguments) == 0 {
					err = command(update.Message.Chat.ID)
				} else {
					err = command(update.Message.Chat.ID, strings.Fields(arguments)...)
				}
				if err != nil {
					bot.logger.Printf("error trying to handle command %v: %v", commandText, err)
				}
			}
		}
	}
	return nil
}
