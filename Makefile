fetch:
	git fetch
	git checkout origin/master

build:
	go build

deploy: fetch build
	systemctl restart fritando.service

format:
	go fmt ./...

run: build
	./fritando
