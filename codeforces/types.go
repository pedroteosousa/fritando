package codeforces

type Contest struct {
	Id    int
	Name  string
	Phase string
}

type AllContestsResponse struct {
	Status string
	Result []Contest
}

type Submission struct {
	ContestId int
}

type UserSubmissionsResponse struct {
	Status string
	Result []Submission
}
