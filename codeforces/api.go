package codeforces

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"time"

	"github.com/patrickmn/go-cache"
)

const (
	allContestsRoute     = "http://codeforces.com/api/contest.list"
	userSubmissionsRoute = "http://codeforces.com/api/user.status?handle=%v"
)

const (
	statusOk      = "OK"
	phaseFinished = "FINISHED"
)

const (
	allContestsCacheKey     = "AllContests"
	userSubmissionsCacheKey = "UserSubmissions;%v"
)

const (
	MAX_RETRIES = 5
)

type CodeforcesAPI interface {
	AllContests() ([]Contest, error)
	UserSubmissions(handle string) ([]Submission, error)
	AvailableContests(handles []string) ([]Contest, error)
}

type Client struct {
	cache *cache.Cache
}

func NewCodeforcesAPIClient(cache *cache.Cache) *Client {
	return &Client{
		cache: cache,
	}
}

func (c *Client) AllContests() ([]Contest, error) {
	if contests, found := c.cache.Get(allContestsCacheKey); found {
		return contests.([]Contest), nil
	} else {
		var response AllContestsResponse
		err := unmarshal(allContestsRoute, &response)
		if response.Status != statusOk {
			return nil, fmt.Errorf("status code: %v", response.Status)
		}
		c.cache.Set(allContestsCacheKey, response.Result, 10*time.Minute)
		return response.Result, err
	}
}

func (c *Client) UserSubmissions(handle string) ([]Submission, error) {
	cacheKey := fmt.Sprintf(userSubmissionsCacheKey, handle)
	if submissions, found := c.cache.Get(cacheKey); found {
		return submissions.([]Submission), nil
	} else {
		var response UserSubmissionsResponse
		err := unmarshal(fmt.Sprintf(userSubmissionsRoute, url.PathEscape(handle)), &response)
		if response.Status != statusOk {
			return nil, fmt.Errorf("status code: %v", response.Status)
		}
		c.cache.Set(cacheKey, response.Result, 10*time.Minute)
		return response.Result, err
	}
}

func (c *Client) AvailableContests(handles []string) ([]Contest, error) {
	contests, err := c.AllContests()
	if err != nil {
		return nil, err
	}
	re := regexp.MustCompile(`(Round \S*)`)
	contestRound := make(map[int]string)
	for _, contest := range contests {
		match := re.FindString(contest.Name)
		if len(match) != 0 {
			contestRound[contest.Id] = match
		}
	}
	unavailableContest := make(map[int]bool)
	unavailableRound := make(map[string]bool)
	for _, handle := range handles {
		submissions, err := c.UserSubmissions(handle)
		if err != nil {
			return nil, err
		}
		for _, submission := range submissions {
			unavailableContest[submission.ContestId] = true
			if round, found := contestRound[submission.ContestId]; found {
				unavailableRound[round] = true
			}
		}
	}
	var availableContests []Contest
	for _, contest := range contests {
		round := contestRound[contest.Id]
		_, contestNotAvailable := unavailableContest[contest.Id]
		_, roundNotAvailable := unavailableRound[round]
		if contestNotAvailable || roundNotAvailable || contest.Phase != phaseFinished {
			continue
		}
		availableContests = append(availableContests, contest)
	}
	return availableContests, nil
}

func unmarshal(url string, object interface{}) error {
	client := &http.Client{}
	var res *http.Response
	var err error

	for retries := 0; retries < MAX_RETRIES; retries += 1 {
		time.Sleep(time.Duration(retries) * time.Second)
		var req *http.Request
		req, err = http.NewRequest("GET", url, nil)
		if err != nil {
			continue
		}
		res, err = client.Do(req)
		if err != nil {
			continue
		}
		if res.StatusCode != http.StatusOK {
			continue
		}
		break
	}

	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("could not complete request to %v, status code = %v", url, res.StatusCode)
	}

	return json.NewDecoder(res.Body).Decode(object)
}
