package commands

import (
	"bytes"
	"fmt"
	"log"
	"regexp"

	"gitlab.com/pedroteosousa/fritando/bot"
	"gitlab.com/pedroteosousa/fritando/codeforces"
)

const numberOfContests = 10
const codeforcesContestURL = "http://codeforces.com/contest/%v"

type FindContest struct {
	bot           bot.Bot
	codeforcesAPI codeforces.CodeforcesAPI
	logger        *log.Logger
}

func NewFindContest(bot bot.Bot, codeforcesAPI codeforces.CodeforcesAPI, logger *log.Logger) *FindContest {
	findContest := &FindContest{
		bot:           bot,
		codeforcesAPI: codeforcesAPI,
		logger:        logger,
	}
	bot.SetCommand("find", findContest.Find)
	bot.SetCommand("find_div1", findContest.FindDiv1)
	bot.SetCommand("find_div2", findContest.FindDiv2)
	return findContest
}

func (action *FindContest) generateMessage(contests []codeforces.Contest) string {
	if len(contests) == 0 {
		return "Não encontrei nenhum contest válido"
	}

	var buffer bytes.Buffer
	for idx, contest := range contests {
		url := fmt.Sprintf(codeforcesContestURL, contest.Id)
		buffer.WriteString(fmt.Sprintf("%v. <a href=\"%v\">%v</a>\n", idx+1, url, contest.Name))
	}
	return buffer.String()
}

func (action *FindContest) Find(chatID int64, args ...string) error {
	contests, err := action.codeforcesAPI.AvailableContests(args)
	if err != nil {
		return err
	}
	contests = contests[:numberOfContests]

	message := action.generateMessage(contests)
	return action.bot.SendMessage(chatID, message)
}

func (action *FindContest) FindDiv1(chatID int64, args ...string) error {
	availableContests, err := action.codeforcesAPI.AvailableContests(args)
	if err != nil {
		return err
	}
	re := regexp.MustCompile(`Div\. 1`)
	var contests []codeforces.Contest
	for _, contest := range availableContests {
		matched := re.Match([]byte(contest.Name))
		if matched {
			contests = append(contests, contest)
		}
	}
	contests = contests[:numberOfContests]

	message := action.generateMessage(contests)
	return action.bot.SendMessage(chatID, message)
}

func (action *FindContest) FindDiv2(chatID int64, args ...string) error {
	availableContests, err := action.codeforcesAPI.AvailableContests(args)
	if err != nil {
		return err
	}
	re := regexp.MustCompile(`Div\. 2`)
	var contests []codeforces.Contest
	for _, contest := range availableContests {
		matched := re.Match([]byte(contest.Name))
		if matched {
			contests = append(contests, contest)
		}
	}
	contests = contests[:numberOfContests]

	message := action.generateMessage(contests)
	return action.bot.SendMessage(chatID, message)
}
